package mh.springKafka.Kafka.repository;

import mh.springKafka.Kafka.tables.pojos.Comment;
import mh.springKafka.Kafka.tables.records.CommentRecord;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.util.List;

import static mh.springKafka.Kafka.tables.Comment.COMMENT;


@Repository
public class CommentRepository {
    private final DSLContext dslContext;

    public CommentRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    //Read data
    public List<Comment> readAll() {
        return dslContext
                .select()
                .from(COMMENT)
                .fetch()
                .into(Comment.class);
    }

    //Create data
    public Integer create(Comment comment) {
        CommentRecord record = dslContext.newRecord(COMMENT, comment);
        return record.store();
    }

    //Update data
    public Integer update(Comment comment) {
        CommentRecord record = dslContext.newRecord(COMMENT, comment);
        return record.store();
    }

    //Delete data
    public Integer delete(String Id) {
        CommentRecord record = dslContext.fetchOne(COMMENT, COMMENT.ID.eq(Id));
        return record.delete();
    }
}
