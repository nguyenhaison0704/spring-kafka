package mh.springKafka.Kafka.config.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import lombok.Data;
import mh.springKafka.Kafka.tables.pojos.Comment;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

@Data
@Component
@Configuration
@ConfigurationProperties("spring.link")
public class GetData {
    private static String link = "https://cdnvda.mhsolution.vn/downloadFile/comment.txt";

    private ObjectMapper objectMapper = new ObjectMapper()
            .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

    @Bean
    public List<Comment> dataFromServer() {
        List<Comment> ob = new ArrayList<>();

        try {
            //Khoi tao ket noi
            URL dataURL = new URL(link);
            URLConnection dc = dataURL.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(dc.getInputStream()));

            //Doc du lieu
            String data;
            while((data = in.readLine()) != null) {
                ob.add(this.getObjectMapper().readValue(data, Comment.class));
            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ob;
    }


}
