package mh.springKafka.Kafka.config.connectMySQL;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.sql.Connection;

@Component
public class MysqlDSLContext {
    private final Connection conn;

    private static DSLContext context;

    public MysqlDSLContext(Connection getConnection) {
        this.conn = getConnection;
    }

    @Bean
    public DSLContext dlsContext() {
        try {
            return context = DSL.using(conn, SQLDialect.MYSQL);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
