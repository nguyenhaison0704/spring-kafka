package mh.springKafka.Kafka.config.connectMySQL;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Data
@Component
@Configuration
@ConfigurationProperties("spring.datasource")
public class MysqlConnection {
    private String url;
    private String username;
    private String password;

    private static Connection conn;

    @Bean
    public Connection getConnection() throws SQLException {
        return conn = DriverManager.getConnection(url, username, password);
    }
}
