package mh.springKafka.Kafka.service.kafka.Producer;

import mh.springKafka.Kafka.tables.pojos.Comment;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.List;

@Component
public class CommentProducer implements ApplicationRunner {

    private final KafkaTemplate<String, Comment> kafkaTemplate;

    private final List<Comment> dataFromServer;

    public CommentProducer(KafkaTemplate<String, Comment> kafkaTemplate, List<Comment> dataFromServer) {
        this.kafkaTemplate = kafkaTemplate;
        this.dataFromServer = dataFromServer;
    }

    public void sendComment(Comment comment, String topicName) {
        ListenableFuture<SendResult<String, Comment>> future =
                kafkaTemplate.send(topicName, comment);

        future.addCallback(new ListenableFutureCallback<SendResult<String, Comment>>() {

            @Override
            public void onSuccess(SendResult<String, Comment> result) {
                System.out.println("Sent comment=[" + comment.toString() +
                        "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Unable to send message=["
                        + comment.toString() + "] due to : " + ex.getMessage());
            }
        });
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        int length = dataFromServer.size();
        for (int i = 0; i < length; i++) {
            this.sendComment(dataFromServer.get(i), "comments");
        }
    }
}
